
var gCoursesDB = {}

$(document).ready(function(){
  onpageloading()
})

function onpageloading(){
  callApiGetServer();
  filterMostPopular(gCoursesDB);
  filterTrending(gCoursesDB);
}

function callApiGetServer() {
  $.ajax({
    url: "/Courses",
    type: "GET",
    dataType: "json",
    async: false,
    success: function (responseJson) {
      gCoursesDB = responseJson.Course;
      console.log(gCoursesDB);
    },
    error: function (ajaxContent) {
      console.log(ajaxContent);
    }
  })
}

// Hàm lọc dữ liệu most popular
function filterMostPopular(paramdata){
   
    var vDataMostPopular = paramdata.filter((item)=>
         item.isPopular == true
    )
    var vMostPopular = "";
    for(var bI =0; bI < vDataMostPopular.length; bI++){
        vMostPopular = vMostPopular + `<div class="col-sm-3 " >
        <div class="card" style="width: 15rem;">
          <img src = ${vDataMostPopular[bI].coverImage} class="card-img-top" alt="...">
          <div class="card-body">
            <a class="card-title">${vDataMostPopular[bI].courseName}</a>
            <p class="card-text"><i class="fas fa-clock"></i> ${vDataMostPopular[bI].duration}  ${vDataMostPopular[bI].level}</p>
            <p class="card-text"> $ ${vDataMostPopular[bI].price}</p>
          </div>
          <div class="card-footer bg-transparent">
            <p  class="navbar-brand form-group" href="#" style="font-size:medium" > 
              <img src = ${vDataMostPopular[bI].teacherPhoto} alt="logo" style="width:40px; border-radius:50%;"> 
              ${vDataMostPopular[bI].teacherName}
            </p>
            <i class="fas fa-bookmark"></i>
          </div>
        </div>
      </div>`
    }
    $(".most-popular").html(vMostPopular);
}
// hàm lọc dữ liệu trending
function filterTrending(paramdata){
    console.log("hàm đã chạy");
    vDataTrending = paramdata.filter((item)=>
    item.isTrending == true
    )
    var vTrending = "";
    for(var bI =0; bI < vDataTrending.length; bI++){
        vTrending = vTrending + `<div class="col-sm-3 " >
        <div class="card" style="width: 15rem;">
          <img src = ${vDataTrending[bI].coverImage} class="card-img-top" alt="...">
          <div class="card-body">
            <a class="card-title">${vDataTrending[bI].courseName}</a>
            <p class="card-text"><i class="fas fa-clock"></i> ${vDataTrending[bI].duration}  ${vDataTrending[bI].level}</p>
            <p class="card-text"> $ ${vDataTrending[bI].price}</p>
          </div>
          <div class="card-footer bg-transparent">
            <p  class="navbar-brand form-group" href="#" style="font-size:medium" > 
              <img src = ${vDataTrending[bI].teacherPhoto} alt="logo" style="width:40px; border-radius:50%;"> 
              ${vDataTrending[bI].teacherName}
            </p>
            <i class="fas fa-bookmark"></i>
          </div>
        </div>
      </div>`
    }
    $(".div-trnding").html(vTrending);
}